import mockStore from '../../../test-utils/mockstore';
import ACTION_TYPES, { getSpacesSuccess } from '../../../redux/actions/space-actions';

/*
    This is an unfinished test suite for unit testing the spaces action creators
    and their functionality with the redux store
*/
describe('Actions: spaces actions',() => {
    const store = mockStore({});

    beforeEach(() => {
        store.clearActions();
    })

    describe('when spaces successfully fetched', () => {
        const expectedAction = {
            type: ACTION_TYPES.GET_SPACES_SUCCESS,
            payload: {
                spaces: ['TEST_SPACE']
            }
        }

        it('should return the expect payload', async () => {
            await store.dispatch(getSpacesSuccess(['TEST_SPACE']));
            expect(store.getActions()).toEqual(expectedAction);

        });
    });
});