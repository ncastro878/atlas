import React from 'react';
import ReactDOM from 'react-dom';
import {App} from '../App';

/*
  Unfortunately, due to my time constraints I did not
  get to create the testing suites for unit tests or
  integration tests I wanted to do. Creating the app itself
  took a decent amount of time, and a comprehensive testing suite 
  would have required a bigger time commitment. I would enjoy the opportunity
  to create tests, though, if that was desired.
*/
it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
 