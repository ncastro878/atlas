import { getAssetsService, getAssetsByIdService } from "../utils/services";

const ACTION_TYPES = Object.freeze({
    GET_ASSETS_ATTEMPT: 'GET_ASSETS_ATTEMPT',
    GET_ASSETS_ERROR: 'GET_ASSETS_ERROR',
    GET_ASSETS_SUCCESS: 'GET_ASSETS_SUCCESS'
});

const getAssetsAttempt = () => {
    return {
        type: ACTION_TYPES.GET_ASSETS_ATTEMPT
    };
}

const getAssetsError = (error) => {
    return{
        type: ACTION_TYPES.GET_ASSETS_ERROR,
        payload: error
    }
}

const getAssetsSuccess = (data) => {
    return{
        type: ACTION_TYPES.GET_ASSETS_SUCCESS,
        payload: data
    }
}

const getAssets = (spaceId) => {
    return dispatch => {
        dispatch(getAssetsAttempt());

        return getAssetsService(spaceId).then(
            response => {
                let {data} = response;
                dispatch(getAssetsSuccess(data));
            },
            error => {
                dispatch(getAssetsError(error));
            }
        )
    }
}

const getAssetsById = (spaceId,assetsId) => {
    return dispatch => {
        dispatch(getAssetsAttempt());

        return getAssetsByIdService(spaceId, assetsId).then(
            response => {
                let {data} = response;
                dispatch(getAssetsSuccess(data));
            },
            error => {
                dispatch(getAssetsError(error));
            }
        )
    }
}

export {
    ACTION_TYPES,
    getAssets,
    getAssetsById
}