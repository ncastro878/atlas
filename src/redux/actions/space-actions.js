import { getSpacesService, getSpacesByIdService } from "../utils/services";
import { getEntries } from "./entries-actions";
import { getAssets } from "./assets-actions";

const ACTION_TYPES = Object.freeze({
    GET_SPACES_ATTEMPT: 'GET_SPACES_ATTEMPT',
    GET_SPACES_ERROR: 'GET_SPACES_ERROR',
    GET_SPACES_SUCCESS: 'GET_SPACES_SUCCESS',
    SET_SELECTED_SPACE: 'SET_SELECTED_SPACE',
    SET_SELECTED_SPACE_ID: 'SET_SELECTED_SPACE_ID'
});

/**
 * One structure I like for my Redux Reducers, Action-Creators, & Store
 * is to keep track of what has been attemped, completed, & failed. That way to track
 * specific errors, render data upon successes, and display loading messages when 
 * attemping to fetch data asynchronously. For this basic app, the ATTEMPT actions may have been
 * unncessary but kept them in case.
 */

const getSpacesAttempt = () => {
    return {
        type: ACTION_TYPES.GET_SPACES_ATTEMPT
    }
}

//this returns the API error to the Store
const getSpacesError = (error) => {
    return {
        type: ACTION_TYPES.GET_SPACES_ERROR,
        payload: error
    }
}

//this returns the array of spaces to the Stores after succesfully hitting the API
const getSpacesSuccess = (data) => {
    return {
        type: ACTION_TYPES.GET_SPACES_SUCCESS,
        payload: data
    }
}

/* 
    This sets the SpaceObject in the store from the one we 
    selected in the left side rail
*/
const setSelectedSpace = (space) => {
    return {
        type: ACTION_TYPES.SET_SELECTED_SPACE,
        payload: space
    }
}

/*
    This sets the SelectedSpace ID in the store for easy retrieval in other components
*/ 
const setSelectedSpaceId = (spaceId) => {
    return {
        type: ACTION_TYPES.SET_SELECTED_SPACE_ID,
        payload: spaceId
    }
}

/**
 * This fetches all Spaces from the API
 */
const getSpaces = () => {
    return dispatch => {
        dispatch(getSpacesAttempt());

        return getSpacesService().then(
            response => {
                let {data} = response;
                console.log('Successful Response', response);
                dispatch(getSpacesSuccess(data));
                //we set a default space to be rendered on the app w/ this function
                dispatch(setDefaultSpaceToDisplay(data));
            }, error =>{
                console.log('Error', error);
                dispatch(getSpacesError(error));
            }
        );
    }
}

/*
    This was never utlized but left just in case
*/
const getSpacesById = (spaceId) => {
    return dispatch => {
        dispatch(getSpacesAttempt());

        return getSpacesByIdService(spaceId).then(
            response => {
                let {data} = response;
                console.log('Successful Response', response);
                dispatch(getSpacesSuccess(data));
            }, error =>{
                console.log('Error', error);
                dispatch(getSpacesError(error));
            }
        );
    }
}

/**
 * This sets the space that is Selected, or in other words, is the one to be rendered fully.
 * We find the space using the spaceId argument to search through all the spaces we fetched.
 * Then we set the ID and the actual space in the Redux Store, and get the accompanying Entries and Assets 
 * that correspond to that Space, so as to render the tables belonging to that Space.
 */
const setNewSelectedSpace = (spaceId) => {
    return (dispatch, getState) => {
        let spaces = getState().spaces.spaces.items;
        let newSpace = spaces.find(space => {
            return space.sys.id === spaceId;
        });
        
        window.history.replaceState(null, '', `/${spaceId}`);

        dispatch(setSelectedSpaceId(spaceId));
        dispatch(setSelectedSpace(newSpace));

        dispatch(getEntries(spaceId));
        dispatch(getAssets(spaceId));
    }
}

/*
    Here we simply set a default spaceto be selected, aka to render.
    We simply go with the first one from out fetched spaces
*/
const setDefaultSpaceToDisplay = (data) => {
    let spaces = data.spaces.items;
    let firstSpaceId = spaces[0].sys.id;
    return dispatch => {    
        dispatch(setNewSelectedSpace(firstSpaceId));
    }
}

export {
    ACTION_TYPES,
    getSpaces,
    getSpacesById,
    getSpacesSuccess,
    setNewSelectedSpace,
    setSelectedSpaceId
}