import { getUserService, getUserByIdService } from "../utils/services";

const ACTION_TYPES = Object.freeze({
    GET_USERS_ATTEMPT: 'GET_USERS_ATTEMPT',
    GET_USERS_ERROR: 'GET_USERS_ERROR',
    GET_USERS_SUCCESS: 'GET_USERS_SUCCESS'
});

/**
 * spaces-actions.js is essentially a template for how our other actions=files are 
 * structured
 */
const getUsersAttempt = () => {
    return {
        type: ACTION_TYPES.GET_USERS_ATTEMPT
    }
}

const getUsersError = (error) => {
    return {
        type: ACTION_TYPES.GET_USERS_ERROR,
        payload: error
    }
}

const getUsersSuccess = (data) => {
    return {
        type: ACTION_TYPES.GET_USERS_SUCCESS,
        payload: data
    }
}


const getUsers = () => {
    return dispatch => {
        dispatch(getUsersAttempt());

        return getUserService().then(
            response => {
                let {data} = response;
                console.log('Successful Response', response);
                dispatch(getUsersSuccess(data.items));
            }, error =>{
                console.log('Error', error);
                dispatch(getUsersError(error));
            }
        );
    }
}

const getUserById = (userId) => {
    return dispatch => {
        dispatch(getUsersAttempt());

        return getUserByIdService(userId).then(
            response => {
                let {data} = response;
                console.log('Successful Response', response);
                dispatch(getUsersSuccess(data));
            }, error =>{
                console.log('Error', error);
                dispatch(getUsersError(error));
            }
        );
    }
}

export {
    ACTION_TYPES,
    getUserById,
    getUsers
}