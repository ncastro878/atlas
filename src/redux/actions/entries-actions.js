import { getEntriesService, getEntriesByIdService } from "../utils/services";

const ACTION_TYPES = Object.freeze({
    GET_ENTRIES_ATTEMPT: 'GET_ENTRIES_ATTEMPT',
    GET_ENTRIES_SUCCESS: 'GET_ENTRIES_SUCCESS',
    GET_ENTRIES_ERROR: 'GET_ENTRIES_ERROR'
});

const getEntriesAttempt = () => {
    return {
        type: ACTION_TYPES.GET_ENTRIES_ATTEMPT
    }
}

const getEntriesError = error => {
    return {
        type: ACTION_TYPES.GET_ENTRIES_ERROR,
        payload: error
    }
}

const getEntriesSuccess = (data) => {
    return {
        type: ACTION_TYPES.GET_ENTRIES_SUCCESS,
        payload: data
    }
}


const getEntries = (spaceId) => {
    return dispatch => {
        dispatch(getEntriesAttempt());

        return getEntriesService(spaceId).then(
            response => {
                let {data} = response;
                dispatch(getEntriesSuccess(data));
            },
            error => {
                dispatch(getEntriesError(error));
            }
        )
    }
}

const getEntriesById = (spaceId, entryId) => {
    return dispatch => {
        dispatch(getEntriesAttempt());

        return getEntriesByIdService(spaceId, entryId).then(
            response => {
                let {data} = response;
                dispatch(getEntriesSuccess(data));
            },
            error => {
                dispatch(getEntriesError(error));
            }
        )
    }
}

export {
    ACTION_TYPES,
    getEntriesById,
    getEntries
}