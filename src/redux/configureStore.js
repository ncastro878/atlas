import {
    applyMiddleware, 
    combineReducers,
    compose,
    createStore 
} from 'redux';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';

// Our Reducers
import assetsReducer from './reducers/assetsReducer';
import entriesReducer from './reducers/entriesReducer';
import spacesReducer from './reducers/spacesReducer';
import usersReducer from './reducers/usersReducer';

export default function configureStore(initialState) {
    const reducer = combineReducers({
        assets: assetsReducer,
        entries: entriesReducer,
        spaces: spacesReducer,
        users: usersReducer
    });

    //the thunk middleware makes our asynchrous API calls a lot easier
    const enhancements = [applyMiddleware(thunk)];

    return createStore(reducer, initialState, composeWithDevTools(...enhancements));
}