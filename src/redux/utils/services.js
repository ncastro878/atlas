import axios from 'axios';

export const getSpacesService = () => {
    const serviceUrl = 'http://localhost:4000/spaceexplorer/space';
    return axios.get(serviceUrl);
};

export const getSpacesByIdService = (spaceId) => {
    const serviceUrl = `http://localhost:4000/spaceexplorer/space/${spaceId}`;
    return axios.get(serviceUrl);
};

export const getAssetsService = (spaceId) => {
    const serviceUrl = `http://localhost:4000/spaceexplorer/space/${spaceId}/assets`;
    return axios.get(serviceUrl);
}

export const getAssetsByIdService = (spaceId, assetsId) => {
    const serviceUrl = `http://localhost:4000/spaceexplorer/space/${spaceId}/assets/${assetsId}`;
    return axios.get(serviceUrl);
}

export const getEntriesService = (spaceId) => {
    const serviceUrl = `http://localhost:4000/spaceexplorer/space/${spaceId}/entries`;
    return axios.get(serviceUrl);
}

export const getEntriesByIdService = (spaceId, entryId) => {
    const serviceUrl = `http://localhost:4000/spaceexplorer/space/${spaceId}/entries/${entryId}`;
    return axios.get(serviceUrl);
}

export const getUserService = () => {
    const serviceUrl = `http://localhost:4000/spaceexplorer/users`;
    return axios.get(serviceUrl);
}

export const getUserByIdService = (userId) => {
    const serviceUrl = `http://localhost:4000/spaceexplorer/users/${userId}`;
    return axios.get(serviceUrl);
}
