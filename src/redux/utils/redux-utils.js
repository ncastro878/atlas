import _ from 'lodash';

export const hasData = (data, property) => {
    if(data) {
        if(_.isArray(data)){
            return data.length > 0;
        }else if(_.isPlainObject(data) && property && !_.isNumber(data[property]) && !_.isBoolean(data[property])){
            return !_.isEmpty(data) && data.hasOwnProperty(property) && !_.isEmpty(data[property]);
        }else if(_.isNumber(data[property]) || _.isBoolean(data[property])){
            return true;
        }else{
            return !_.isEmpty(data);
        }
    }
    return false;
};