import {ACTION_TYPES} from '../actions/space-actions';

const initialState = Object.freeze({
    isComplete: false,
    isRetrieving: false,
    error: null,
    selectedSpace: {},
    selectedSpaceId: null,
    spaces: {}
});

/**
 * We update our Spaces Reducer with a few options: with a list of spaces retrieved,
 * with 1 space that is the "SelectedSpace" to render, with an Error, or with a notice of
 * our attempt to retrive all space from the API.
 */
export default function SpacesReducer (state = initialState, action) {
    switch(action.type) {
        case ACTION_TYPES.GET_SPACES_ATTEMPT:{
            return{
                ...state,
                isComplete: false,
                isRetrieving: true
            }
        }
        case ACTION_TYPES.GET_SPACES_ERROR: {
            return{
                ...state,
                error: action.payload,
                isComplete: false,
                isRetrieving: false
            }
        }
        case ACTION_TYPES.GET_SPACES_SUCCESS: {
            return { 
                ...state,
                isComplete: true,
                isRetrieving: false,
                spaces: action.payload.spaces,
            }
        } 
        case ACTION_TYPES.SET_SELECTED_SPACE: {
            return {
                ...state,
                selectedSpace: action.payload
            }
        }
        case ACTION_TYPES.SET_SELECTED_SPACE_ID:{
            return {
                ...state,
                selectedSpaceId: action.payload
            }
        }
        default: 
            return state;
    }
};
