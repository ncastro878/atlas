import {ACTION_TYPES} from '../actions/assets-actions';

const initialState = Object.freeze({
    isComplete: false,
    isRetrieving: false,
    error: null,
    assets: {}  
});

/**
 * This Reducer tracks our attempt to get all Assets, our Success of getting
 * all assets + an array of the asses, or our failure of getting all assets + the error
 */
const AssetsReducer = (state = initialState, action) => {
    switch(action.type){
        case ACTION_TYPES.GET_ASSETS_ATTEMPT: {
            return{
                ...state,
                isComplete: false,
                isRetrieving: true
            }
        }
        case ACTION_TYPES.GET_ASSETS_ERROR: {
            return{
                ...state,
                error: action.payload,
                isComplete: false,
                isRetrieving: false
            }
        }
        case ACTION_TYPES.GET_ASSETS_SUCCESS: {
            return{ 
                ...state,
                isComplete: true,
                isRetrieving: false,
                assets: action.payload,
            }
        }
        default: return state;
    }
}

export default AssetsReducer;