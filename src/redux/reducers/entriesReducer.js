import {ACTION_TYPES} from '../actions/entries-actions';

const initialState = Object.freeze({
    isComplete: false,
    isRetrieving: false,
    error: null,
    entries: {}  
});

/**
 * This reducer tracks 3 things: our attempt to get all entries,
 * our success of getting all entries + the entries themselves, 
 * or an error from a failed attempt at getting the entries
 */
const EntriesReducer = (state = initialState, action) => {
    switch(action.type){
        case ACTION_TYPES.GET_ENTRIES_ATTEMPT: {
            return{
                ...state,
                isComplete: false,
                isRetrieving: true
            }
        }
        case ACTION_TYPES.GET_ENTRIES_ERROR: {
            return{
                ...state,
                error: action.payload,
                isComplete: false,
                isRetrieving: false
            }
        }
        case ACTION_TYPES.GET_ENTRIES_SUCCESS: {
            return{ 
                ...state,
                isComplete: true,
                isRetrieving: false,
                entries: action.payload,
            }
        }
        default: return state;
    }
}

export default EntriesReducer;