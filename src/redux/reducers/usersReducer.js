import {ACTION_TYPES} from '../actions/users-actions';

const initialState = Object.freeze({
    isComplete: false,
    isRetrieving: false,
    error: null,
    users: {}  
});

/**
 * This reducer makes note of our attempt to get all Users,
 * our Success of getting all users + the Users, or our failure
 * of getting all the users + the error
 */
const UsersReducer = (state = initialState, action) => {
    switch(action.type){
        case ACTION_TYPES.GET_USERS_ATTEMPT: {
            return{
                ...state,
                isComplete: false,
                isRetrieving: true
            }
        }
        case ACTION_TYPES.GET_USERS_ERROR: {
            return{
                ...state,
                error: action.payload,
                isComplete: false,
                isRetrieving: false
            }
        }
        case ACTION_TYPES.GET_USERS_SUCCESS: {
            return{ 
                ...state,
                isComplete: true,
                isRetrieving: false,
                users: action.payload,
            }
        }
        default: return state;
    }
}

export default UsersReducer;