import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Tabs, Tab, Table} from 'react-bootstrap';
import {hasData} from '../../redux/utils/redux-utils';
import {generateKey} from '../utils/table-utils';

/**
 * This component required the most work, and would be very fun to go through and refactor,
 *  but what can you do? Anyways, this renders the Entries & Assets table from the SelectedSpace.
 * This required he most work to properly render the table rows and implement sorting.
 */
export class SpaceTable extends Component{
    /**
     * We use state to keep track of which table attribute to sort by and which direction.
     * We track for both the Entries table and the Assets table
     */
    state = {
        assetSortType: 'title',
        entrySortType: 'title',
        assetDirectionUp: true,
        entryDirectionUp: true 
    }

    //we set the Asset table sort attribute & direction here
    setAssetSort = (sortType) => {
        this.setState(prevState => ({
            assetSortType: sortType,
            assetDirectionUp: !prevState.assetDirectionUp
        }));
    }

    //we set the Entry table sort attribute & direction here
    setEntrySort = (sortType) => {
        this.setState(prevState => ({
            entrySortType: sortType,
            entryDirectionUp: !prevState.entryDirectionUp
        }));
    }

    /**
     * We render the table rows for assets here using the assets we fetched from the API, then
     * filtering through them for those that match our SelectedSpace. We then sort those assets.
     * If 0 assets match the space, we present a small "Assets not available" msg.
     * Otherwise, we map thru the assets and create a list of table rows with table data,
     * while also giving them unique keys as required in lists. 
     */
    renderAssetsRows = () => {
        let {assets, selectedSpaceId} = this.props;
        let filteredAssets = assets.filter(asset => {
            return asset.sys.space === selectedSpaceId;
        });
        
        filteredAssets = this.sortAssets(filteredAssets);

        if(filteredAssets.length === 0) {
            return(<tr><td>Assets Not Available</td></tr>);
        }
        return filteredAssets.map((asset, index) => {
            return(
                <tr key={generateKey() * index+1}>
                    <td key={generateKey() * index+2}>{asset.fields.title}</td>
                    <td key={generateKey() * index+3}>{asset.fields.contentType}</td>
                    <td key={generateKey() * index+4}>{asset.fields.fileName}</td>
                    <td key={generateKey() * index+5}>{this.renderUsername(asset.sys.createdBy)}</td>
                    <td key={generateKey() * index+6}>{asset.sys.updatedAt}</td>
                    <td key={generateKey() * index+7}>{this.renderUsername(asset.sys.updatedBy)}</td>
                </tr>
            );
        });
    }

    /**
     * We render the table headers and the table, and outsource the row creation to
     * another function
     */
    renderAssetsTable = () => {
        return(
            <Table>
            <thead>
            <tr>
                <th onClick={() => this.setAssetSort('title')}>Title</th>
                <th onClick={() => this.setAssetSort('contentType')}>Content Type</th>
                <th onClick={() => this.setAssetSort('fileName')}>File Name</th>
                <th onClick={() => this.setAssetSort('createdBy')}>Created By</th>
                <th onClick={() => this.setAssetSort('updatedAt')}>Updated At</th>
                <th onClick={() => this.setAssetSort('updatedBy')}>Updated By</th>
            </tr>
            </thead>
            <tbody>
                {
                    this.renderAssetsRows()
                }
            </tbody>
            </Table>
        );
    }

    /**
     * This could be optimized to be cleaner if more time allowed, but it functions properly given 
     * the constraints and was a fun problem. It takes in an array of Entries, then takes note of which
     * attribute to Sort by and which Direction, from state. We have conditional statement, because some 
     * attribute such as 'title' and 'summary' reside in different parts of the Entry object than'createdBy'.
     * We use the Array.sort() method to compare the specific attribute of Entries, and at the end, if our 
     * sort Direction is pointed in the opposite direction(aka false), then we reverse our already sorted array.
     * We also had to consider the fact that the "createdBy" & "updatedBy" values of entries use the unique ID's
     * of users, so have to be converted to their English names with our renderUsername() function before sorting.
     */
    sortEntries = (entries) => {
        let {entrySortType, entryDirectionUp} = this.state;
        if(entrySortType === 'title' || entrySortType === 'summary'){
            let sortedEntries = entries.sort((a,b) => {
                return a.fields[entrySortType].localeCompare(b.fields[entrySortType]);
            });
            if(!entryDirectionUp){
                return sortedEntries.reverse();
            }
            return sortedEntries;
        }else if(entrySortType === 'createdBy' || entrySortType === 'updatedBy'){
            let sortedEntries = entries.sort((a,b) => {
                let nameA = this.renderUsername(a.sys[entrySortType]);
                let nameB = this.renderUsername(b.sys[entrySortType]);
                return nameA.localeCompare(nameB);
            });
            if(!entryDirectionUp){
                return sortedEntries.reverse();
            }
            return sortedEntries;
        }
        //updatedAt or updatedBy are the entrySortType at this point
        let sortedEntries = entries.sort((a,b) => {
            return a.sys[entrySortType].localeCompare(b.sys[entrySortType]);
        });
        if(!entryDirectionUp){
            return sortedEntries.reverse();
        }
        return sortedEntries;
    }

    /**
     * This functions exactly as the sortEntries() function, only for assets and their
     * specific object attributes
     */
    sortAssets = (assets) => {
        let {assetSortType, assetDirectionUp} = this.state;
        if(assetSortType === 'title' || assetSortType === 'contentType' || assetSortType === 'fileName'){
            let sortedAssets = assets.sort((a,b) => {
               return a.fields[assetSortType].localeCompare(b.fields[assetSortType]);
            }, {sensetivity:'base'});
            if(!assetDirectionUp){
                return sortedAssets.reverse();
            }
            return sortedAssets;
        }else if(assetSortType === 'createdBy' || assetSortType === 'updatedBy'){
            let sortedAssets = assets.sort((a,b) => {
                let nameA = this.renderUsername(a.sys[assetSortType]);
                let nameB = this.renderUsername(b.sys[assetSortType]);
                return nameA.localeCompare(nameB);
            });
            if(!assetDirectionUp){
                return sortedAssets.reverse();
            }
            return sortedAssets;
        }
        let sortedAssets = assets.sort((a,b) => {
            return a.sys[assetSortType].localeCompare(b.sys[assetSortType]);
        });
        if(!assetDirectionUp){
            return sortedAssets.reverse();
        }
        return sortedAssets;
    }

    /*
        functions exactly as renderAssetsRows()
    */
    renderEntriesRows = () => {
        let {entries, selectedSpaceId} = this.props;

        let filteredEntries = entries.filter(entry => {
            return entry.sys.space === selectedSpaceId;
        });

        filteredEntries = this.sortEntries(filteredEntries);

        if(filteredEntries.length === 0) {
            return(<tr><td>Entries Not Available</td></tr>);
        }

        /*
            the generateKey() function uses seconds since epoch time to create a
            number for list items, using that items index as well.
        */
        return filteredEntries.map((entry, index) => {
            return(
                <tr key={generateKey() * index +1}>
                    <td key={generateKey() * index+2}>{entry.fields.title}</td>
                    <td key={generateKey() * index+3}>{entry.fields.summary}</td>
                    <td key={generateKey() * index+4}>{entry.sys.createdAt}</td>
                    <td key={generateKey() * index+5}>{this.renderUsername(entry.sys.createdBy)}</td>
                    <td key={generateKey() * index+6}>{entry.sys.updatedAt}</td>
                    <td key={generateKey() * index+7}>{this.renderUsername(entry.sys.updatedBy)}</td>
                </tr>
            );
        });
    }

    renderEntriesTable = () => {
        return( 
            <Table>
            <thead>
            <tr>
                <th onClick={() => this.setEntrySort('title')}>Title</th>
                <th onClick={() => this.setEntrySort('summary')}>Summary</th>
                <th onClick={() => this.setEntrySort('createdAt')}>Created At</th>
                <th onClick={() => this.setEntrySort('createdBy')}>Created By</th>
                <th onClick={() => this.setEntrySort('updatedAt')}>Updated At</th>
                <th onClick={() => this.setEntrySort('updatedBy')}>Updated By</th>
            </tr>
            </thead>
            <tbody>
                {
                    this.renderEntriesRows()
                }
            </tbody>
            </Table>
        );
    }

    /**
     * this takes in a user's uniqueId("4FLrUHftHW3v2BLi9fzfjU"), and finds the matching 
     * name to it, such as "Alana Atlassian" from the list of returned Users.
     */
    renderUsername = (userId) => {
        let {users} = this.props;
        
        let user = users.find(user => {
            return user.sys.id === userId
        });
        return user.fields.name;
    }

    render(){
        return(
            <Tabs className="the-tabs">
                <Tab title="Entries" eventKey="entries" className="the-tab">
                    {
                        this.renderEntriesTable()
                    }
                </Tab>
                <Tab title="Assets" eventKey="assets" className="the-tab">
                    {
                        this.renderAssetsTable()
                    }
                </Tab>
            </Tabs>
        );
    }
};

const mapStateToProps = state => {
    return {
        assets: hasData(state.assets, 'assets') ? state.assets.assets: [],
        entries: hasData(state.entries, 'entries') ? state.entries.entries: [],
        selectedSpaceId: hasData(state.spaces, 'selectedSpaceId')? state.spaces.selectedSpaceId: '',
        spaces: state.spaces,
        users: hasData(state.users, 'users') ? state.users.users : []
    }
};
  
export default connect(mapStateToProps)(SpaceTable);;