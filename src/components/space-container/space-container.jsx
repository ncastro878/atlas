import React from 'react';
import SpaceDisplay from '../space-display/space-display';
import SpaceTable from '../space-table/space-table';
import {Col} from 'react-bootstrap';

/*
    A stateless component that merely houses 2 stateful components together
*/
export const SpaceContainer = props => {
    return(
        <Col sm={10}> 
            <SpaceDisplay/>
            <SpaceTable />
        </Col>
    );
};