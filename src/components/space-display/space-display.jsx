import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Col, Row} from 'react-bootstrap';
import {hasData} from '../../redux/utils/redux-utils';

/**
 * This component renders the Title, Auhor, & Summary of the Selected Space 
 * to render on the app. All of this happends above the table.
 */
export class SpaceDisplay extends Component {
    /*
        We fetch the description property/value from the SelectedSpace 
        in our props, which was mapped from the Redux-Store
    */
    renderDescription = () => {
        let {selectedSpace} = this.props;
        if(hasData(selectedSpace, 'fields')){
            return selectedSpace.fields.description;
        }
    }

    //We fetch the title property from the SelectedSpace
    renderTitle = () => {
        let {selectedSpace} = this.props;
        if(hasData(selectedSpace, 'fields')){
            return selectedSpace.fields.title;
        }
    }

    /*
        We check the createdBy id in the SelectedSpace, and search thru the returned array of users
        for a user with the matching id, then return that User's Name.
    */
    renderUserName = () => {
        let {users, selectedSpace} = this.props;
        let uniqueUserId = hasData(selectedSpace.sys, 'createdBy')? selectedSpace.sys.createdBy: '';
        if(users.length > 0){
            let user = users.find(user => {
                return user.sys.id === uniqueUserId;
            });
            return user.fields.name;
        }
    } 

    /*
    We either render a small error message if necessary
    or simply render the Title, Author, & Summary
    */
    render(){
        let {error} = this.props;
        return(
            <div>
                {
                    error !== null &&
                    <h1 className="header">An Error has occured, please try again.</h1>
                }
                {
                    error === null &&
                    <div>
                        <div> 
                            <h1 className="header ">
                                {  
                                    this.renderTitle()
                                }
                            </h1>
                            <h5 className="header ">
                                Created By {this.renderUserName()}
                            </h5>
                        </div>     
                        <Row>
                            <Col sm="6" className="display-section">
                                <div>                           
                                    {
                                        this.renderDescription()
                                    }
                                </div>
                            </Col>
                        </Row>
                    </div>
                }
            </div>
        );
    }
}

/**
 * Our Redux-Store houses the SelectedSpace to render,
 * as well as houses the list of returned spaces, users, and checks if an error was returned
 * from a service call.
 */
const mapStateToProps = state => {
    const {users} = state;
    let {spaces} = state.spaces;
    let errorExists = hasData(state.spaces, 'error') || hasData(state.assets, 'error') ||
                        hasData(state.users, 'error') || hasData(state.entries, 'error');
    return {
        error: errorExists ? 'An error has occured.': null,
        selectedSpace: state.spaces.selectedSpace,
        spaces: hasData(spaces, 'items') ? spaces.items : null,
        users: hasData(users, 'users') ? users.users : []
    }
  };
  
  export default connect(mapStateToProps)(SpaceDisplay);;