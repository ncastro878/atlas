import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Nav, Col} from 'react-bootstrap';
import {setNewSelectedSpace} from '../../redux/actions/space-actions';
import logo from '../../css/atlassian-logo.svg';

export class LeftRail extends Component {

    /*
        When a list-item(a space) is clicked, we will set that space as 
        "SelectedSpace" to display, or render on the app
    */
    handleClick = (spaceId) =>{
        const {dispatch} = this.props;
        dispatch(setNewSelectedSpace(spaceId));
    }

    renderListOfSpaces = () => {
        const {spaces} = this.props;

        return spaces && spaces.map(space => {
            const {title} = space.fields;
            const {id} = space.sys;

            return(
                <Nav.Link as="li"  key={id} onClick={() => this.handleClick(id)} className="left-rail-tiles">
                        {title}
                </Nav.Link>
            );
        });
    }

    render(){
        return(
            <Col sm={2} className="left-rail">
                <img src={logo} alt="Logo" className="logo"/>
                <Nav className="flex-column" >
                    {this.renderListOfSpaces()}
                </Nav>
            </Col>
        );
    }
}

const mapStateToProps = state => {
    const {items} = state.spaces.spaces;
    return {
      spaces: items === undefined ? null : items
    }
};
  
export default connect(mapStateToProps)(LeftRail);;