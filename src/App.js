import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getSpaces} from './redux/actions/space-actions';

import {Container, Col, Row} from 'react-bootstrap';
import LeftRail from './components/left-rail/left-rail';
import {SpaceContainer} from './components/space-container/space-container';
import {getUsers} from './redux/actions/users-actions';

class App extends Component {

  static propTypes = {
    spaces: PropTypes.object
  };

  componentDidMount(){
    let {dispatch} = this.props;

    dispatch(getSpaces());
    dispatch(getUsers());
  }

  render(){
    return (
      <Container>
        <Row>
          <LeftRail/>
          <SpaceContainer/>
        </Row>
      </Container>
    );
  }
};

const mapStateToProps = state => {
  return {
    spaces: state.spaces
  }
};

export default connect(mapStateToProps)(App);;