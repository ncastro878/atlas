const express = require('express');
const app = express();
const port = 4000;
const cors = require('cors');

const spaces = require('./spaces');
const entries = require('./entries');
const assets = require('./assets');
const users = require('./users');

/**
 * This is a crude server that serves the Mock Data provied in the RAML files.
 * It uses Node-Express. There is no real structure to it, it merely serves the 
 * proper files and/or data based on the endpoints hit. It is to be used in development
 * and for testing the built product.
 */

app.use(cors());

app.get('/spaceexplorer/', (req, res) => res.redirect('/spaceexplorer/space/'));

app.get('/spaceexplorer/space/', (req, res) => {
    res.json(spaces);
});
app.get('/spaceexplorer/space/:spaceId', (req, res) => {
    let spaceId = req.params.spaceId;
    let spacesArray = spaces.items; 
    let desiredSpace = spacesArray.find(space => {
        return space.sys.id === spaceId;
    });
    res.json(desiredSpace);
});

app.get('/spaceexplorer/space/:spaceId/entries', (req, res) => {
    let spaceId = req.params.spaceId;
    let entriesArray = entries.entries.items;
    let desiredEntries = entriesArray.filter(entry => {
        return entry.sys.space === spaceId;
    });
    res.json(desiredEntries);
});
app.get('/spaceexplorer/space/:spaceId/entries/:entryId', (req, res) => {
    let spaceId = req.params.spaceId;
    let entryId = req.params.entryId;
    let entriesArray = entries.entries.items;
    let desiredEntry = entriesArray.find(entry => {
        return entry.sys.space === spaceId && 
                entry.sys.id === entryId;
    });
    res.send(desiredEntry);
});

app.get('/spaceexplorer/space/:spaceId/assets', (req, res) => {
    let spaceId = req.params.spaceId;
    let assetsArray = assets.assets.items;
    let desiredAssets = assetsArray.filter(asset => {
        return asset.sys.space === spaceId;
    });
    res.send(desiredAssets);
});
app.get('/spaceexplorer/space/:spaceId/assets/:assetsId', (req, res) => {
    let spaceId = req.params.spaceId;
    let assetId = req.params.assetsId;
    let assetsArray = assets.assets.items;
    let desiredAsset = assetsArray.find(asset => {
        return asset.sys.space === spaceId && 
        asset.sys.id === assetId;
    });
    res.json(desiredAsset);
});

app.get('/spaceexplorer/users', (req, res) => {
    res.send(users);
});

app.get('/spaceexplorer/*', (req, res) => {
    res.status(404).send('404: Error');
});
app.listen(port, () => console.log(`Mock Spaces running on port ${port}!`));