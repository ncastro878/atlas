
## How to Build & Run the app

1. Navigate to a directory to clone the repo, then run these commands
2. ```git clone https://bitbucket.org/ncastro878/atlas.git``` 
3. ```cd atlas ```
4. ```npm install```
5. ```npm run build```
6. ```npm install -g serve```
7. ```npm run concurrent```
8. Now navigate to http://localhost:5000
9. The app and mock-server should be running concurrently and the app should be running at the mentioned address, while hitting the locally run mock-server

---

##Notes:

This was a really fun project to build, and despite it being simple in functionality, there are lots more possible ways of implementing these and other features and functions to make this app more optimal and the cleaner and leaner. 

I built this app using React & Redux for the brunt of the work, and a few other npm packages for other necessary jobs. I used Node-Express to create the server that hosts the mock api data from the raml files. After building the project, a script is ran to concurrently host the site locally as well as run the mock server(and mock api files).

Rather than styling my own elements, I borrowed components from the pre-formatted React-Bootstrap library, such as their Navigation Bar, and Table.
